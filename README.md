# Desarrollo

requerimientos
Java 1.8
org.springframework.boot:spring-boot-starter-data-jpa
org.springframework.boot:spring-boot-starter-security
org.springframework.boot:spring-boot-starter-web
mysql:mysql-connector-java
io.jsonwebtoken', name: 'jjwt', version: '0.9.1'
springBootVersion = '1.5.3.RELEASE'


Crear base datos  mysql

CREATE DATABASE `bootdb` /*!40100 DEFAULT CHARACTER SET latin1 */;

CREATE TABLE `phone` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `citycode` int(11) DEFAULT NULL,
  `contrycode` int(11) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKb0niws2cd0doybhib6srpb5hh` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `isactive` bit(1) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;



Prueba:
register:Registro de usuarios
http://localhost:8080/register
body :{
"username": "admin",
"password": "22aAa",
"email":"admin@admin.cl",
"phones":[
	{
	number:11223344, 
	citycode:56, 
	contrycode:02
	},
	{
	number:111112222, 
	citycode:56, 
	contrycode:02
	}
	]
}

retorno:

{"token":"234234234qssd9u9sdu923e0923ue023423423etc"}


authenticate:Autenticación de usuario
http://localhost:8080/authenticate
body :{
"username": "admin",
"password": "22aAa"
}
	
retorno:

{"token ":1,created:"19-12-2019", isactive:true, last_login:"19-12-2019",modified:"19-12-2019"}




