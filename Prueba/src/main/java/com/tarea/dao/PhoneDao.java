package com.tarea.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.tarea.model.DAOPhone;

@Repository
public interface PhoneDao extends CrudRepository<DAOPhone, Integer> {
	
	
	
}