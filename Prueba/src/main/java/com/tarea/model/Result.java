package com.tarea.model;

import java.io.Serializable;
import java.util.Date;

public class Result  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String mensaje;
	Integer id;
	Date created;
	Date modified;
	Date last_login;
	String token;
	Boolean isactive;
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public Date getModified() {
		return modified;
	}
	public void setModified(Date modified) {
		this.modified = modified;
	}
	public Date getLast_login() {
		return last_login;
	}
	public void setLast_login(Date last_login) {
		this.last_login = last_login;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public Boolean getIsactive() {
		return isactive;
	}
	public void setIsactive(Boolean boolean1) {
		this.isactive = boolean1;
	}
	
}
