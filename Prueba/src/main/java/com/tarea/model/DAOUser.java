package com.tarea.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tarea.dao.PhoneDao;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "user")
public class DAOUser {

	 @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
	    private List<DAOPhone> phones;

		
		public List<DAOPhone> getPhones() {
			return phones;
		}
		public void setPhones(List<DAOPhone> phones) {
			this.phones = phones;
		}
		
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@Column
	private String username;
	@Column
	@JsonIgnore
	private String password;
	@Column
	private String email;
	
	private Date created;
	private Date modified;
	private Boolean isactive;
	private Date last_login;
	
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getModified() {
		return modified;
	}

	public void setModified(Date modified) {
		this.modified = modified;
	}

	public Boolean getIsactive() {
		return isactive;
	}

	public void setIsactive(Boolean isactive) {
		this.isactive = isactive;
	}

	public Date getLast_login() {
		return last_login;
	}

	public void setLast_login(Date last_login) {
		this.last_login = last_login;
	}
	public Integer getId() {
		
		return (int) id;
	}

}