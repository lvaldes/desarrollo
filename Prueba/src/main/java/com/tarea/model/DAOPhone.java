package com.tarea.model;


import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.mapping.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;
 
@Entity
@Table(name = "phone")
public class DAOPhone  implements Serializable {
     
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	Long  id;
    
    Integer number;
    Integer citycode;
    Integer contrycode;
  

	@ManyToOne
	@JoinColumn(name="user_id", nullable=false)
	private DAOUser user;
	
    
    public Long  getId() {
        return id;
    }
    public void setId(Long  id) {
        this.id = id;
    }
	public Integer getNumber() {
		return number;
	}
	public void setNumber(Integer number) {
		this.number = number;
	}
	public Integer getCitycode() {
		return citycode;
	}
	public void setCitycode(Integer citycode) {
		this.citycode = citycode;
	}
	public Integer getContrycode() {
		return contrycode;
	}
	public void setContrycode(Integer contrycode) {
		this.contrycode = contrycode;
	}
	//public Users getUsers() {
	//	return users;
	//}
	public void setUser(DAOUser user) {
		this.user = user;
	}
    
	 @Override
	    public String toString() {
	        return "Phone [id=" + id + ", number=" + number + ", citycode=" + citycode + ", contrycode=" + contrycode + "]";
	    }
	 
 
}