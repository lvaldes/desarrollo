package com.tarea.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.tarea.dao.PhoneDao;
import com.tarea.dao.UserDao;
import com.tarea.model.DAOPhone;
import com.tarea.model.DAOUser;
import com.tarea.model.PhoneDTO;
import com.tarea.model.Result;
import com.tarea.model.UserDTO;

@Service
public class JwtUserDetailsService implements UserDetailsService {
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private PhoneDao phoneDao;

	@Autowired
	private PasswordEncoder bcryptEncoder;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		DAOUser user = userDao.findByUsername(username);
		if (user == null) {
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
				new ArrayList<>());
	}
	
    Pattern pattern;
	Matcher matcher;
   final String EMAIL_REGEX = "^[\\w-\\+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})$";
   final String PASSWORD_REGX="^(?=(?:.*\\d){2,2})(?=(?:.*[A-Z]){1,1})(?=(?:.*[a-z]){1})\\S{4,5}$";

	 public boolean validateEmail(String email) {
	    	pattern = Pattern.compile(EMAIL_REGEX, Pattern.CASE_INSENSITIVE);
	    	matcher = pattern.matcher(email);
			return matcher.matches();
		}

	 public boolean validatePassword(String password) {
	    	pattern = Pattern.compile(PASSWORD_REGX, Pattern.CASE_INSENSITIVE);
	    	matcher = pattern.matcher(password);
			return matcher.matches();
		}

	
	public Result save(UserDTO user) {
		Result result=new Result();
		Iterable<DAOUser> usuarios = userDao.findAll();
		result.setMensaje("0");
		//email ya existe
		for(DAOUser u: usuarios) {
			if(u.getEmail().equals(user.getEmail())) {
				System.out.println(u.getEmail()+" "+user.getEmail());
				result.setMensaje("error: El correo ya esta registrado.");
				break;
			}
		}
		
	
		if(result.getMensaje().length()<=1) {
		if(validateEmail(user.getEmail()))
		result.setMensaje("0");
		else
		result.setMensaje("error: No cumple el patron para un email");
		}
		
		//email no cumple patron de expresion regular
		
		if(result.getMensaje().length()<=1) {
			
		if(validatePassword(user.getPassword()))
		result.setMensaje("0");
		else
		result.setMensaje("error: La password debe cumple una Mayuscula, letras minusculas, y dos numeros");
		}
		
		if(result.getMensaje().equals("0")) {
		DAOUser newUser = new DAOUser();		
		newUser.setUsername(user.getUsername());
		newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
		newUser.setCreated(new Date());
		newUser.setModified(new Date());
		newUser.setLast_login(new Date());
		newUser.setIsactive(true);
		newUser.setEmail(user.getEmail());
			
		DAOUser user_new=  userDao.save(newUser);
		
		List <PhoneDTO> phones = new ArrayList<PhoneDTO>();
		phones = user.getPhones();	
		for(int i = 0; i<phones.size();i++) {
		DAOPhone phone = new DAOPhone();
		phone.setNumber(phones.get(i).getNumber());	
		phone.setCitycode(phones.get(i).getCitycode());
		phone.setContrycode(phones.get(i).getContrycode());
		phone.setUser(newUser);
		DAOPhone phone_new = phoneDao.save(phone);
		}
		result.setId(user_new.getId());
		result.setIsactive(user_new.getIsactive());
		result.setLast_login(user_new.getLast_login());
		result.setModified(user_new.getModified());
		result.setCreated(user_new.getCreated());
		result.setMensaje("Exito");
		
		
		}
		return result;
	}
}