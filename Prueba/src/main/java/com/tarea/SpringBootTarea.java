package com.tarea;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootTarea {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootTarea.class, args);
	}
}